# Lophelia MultiStress RNASeq Analysis

 Repository for data analysis from Chapter 2 of my dissertation (submitted to Molecular Ecology) - Lophelia RNASeq response to Multistressors 

## Files 
#### Step 1: **Full_DESeq.R** - script walking through the DESeq analysis and outputs. Most of the iteratons are documented to determine the influence of treatment, experiment number, and genotype (in that order). 
* __Inputs__: 
  * T1 - Contains directories where all of the ".quant" files are located for each sample
  * overLapper_original.R - script need to generate the VennDiagrams 
  * loph_gene_name_id.csv - a csv file generated in dammit with all of the gene names. Columns = position, seqIid, Name
* __Outputs__:
  * DESeq_outputs - files generated from running through the Full_DESeq.R script including .csv files for the significant differentially expressed genes depending on the different factors (treatment, experiment number, genotype) and certain interactions. Also a panel of a PCA plot for the different factors (treatment, experiment number, genotype). Other plots were not saved into repository.

#### Step 2: **Full_WGCNA.R** - script walking through the WGCNA analysis (uses DESeq objects as input) and outputs 
* __Inputs__: 
  * T1 - Contains directories where all of the ".quant" files are located for each sample. Used to generate "dds" object if not running directly into this script from the Full_DESeq.R script. Yes, I could have same this as a .RData file but I'm still learning and I didn't do that for this project. 
  * loph_gene_name_id.csv - a csv file generated in dammit with all of the gene names. Columns = position, seqIid, Name. Needed for the same reason as above. 
  * traitData.csv - needed for the outlier detection step. 
  * SamplesAndTraits_signed.RData - generated during the process of this script but read in to run the network construction and module detection step

* __Outputs__:
  * moduleColors_0.25.csv - csv of all of the module/color data when merging modules that are 75% similar
  * MergeNetwork_sft20signed_Merg0.25 - save module colors and labels for use in subsequent parts of the script 
  * AnnotatedNetworkAnalysisResults_rlog_signed_Merge25_sft20.csv - csv of massive table of all information - module membership, genes, gene names, etc.
  * Outputs for each color module to be used for the GO_MWU analysis: (exp_num/treatment/pH/temp)_vsd_MM_(modulecolor).csv, (exp_num/treatment/pH/temp)_vsd_MM_(modulecolor)_categorical.csv

#### Step 3:  **Full_GO_MWU.R** - script walking through the GO_MWU analysis (uses WGCNA output as input) and outputs 
* __Inputs__: 
  * File generated by WGCNA script - exp_num/treatment/pH/temp_vsd_MM_modulecolor_categorical.csv, ex., pH_GO_MM_brown_categorical.csv
  * Lophelia_gene2go.txt - annotation file
  * gomwu.functions.R - make sure to download the whole GO_MWU file from https://github.com/z0on/GO_MWU

* __Outputs__:
  * sig_ordered_MWU_(BP/CC/MF)_(Treatment/temp/pH/exp_num)_GO_MM_(modulecolor).csv. ex., sig_ordered_MWU_BP_Treatment_GO_MMlightgreen_categorical_25.csv
  * MWU_(BP/CC/MF)_(modulecolor).csv. ex., MWU_CC_turquoise.csv

#### Step 4: **filtering_terms_for_metacoder.R** - script for using GO_MWU output to generate metacoder plots 

* __Inputs__: 
  * (modulecolor)_unique_GO_terms.csv - list of unique GO terms for each color module
  * data_temp_(modulecolor).csv - generated during the script half way through and has to be reloaded for the second half of the script 
  
* __Outputs__:
  * Metacoder plots - metacoder plots generated for each color. The plots are then modified using Adobe Illustrator. 

